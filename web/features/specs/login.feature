#language:pt

Funcionalidade: Login
    Sendo um usuário cadastrado
    Quero acessar o sistema da Rocklov
    Para que eu possa anunciar meus equipamentos musicais

    @login
    Cenario: Login do usuário

        Dado que acesso a página principal
        Quando submeto minhas credenciais com "albert@gmail.com" e "123456"
            Então sou redirecionado para o Dashboard

            Esquema do Cenário: Tentar logar

        Dado que acesso a página principal
        Quando submeto minhas credenciais com "<email_input>" e "<senha_input>"
            Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
            | email_input         | senha_input | mensagem_output                  |
            | daniel@gmail.com    | 12456       | Usuário e/ou senha inválidos.    |
            | danielson@gmail.com | 1234        | Usuário e/ou senha inválidos.    |
            | daniel¨&gmail.com   | 1234        | Oops. Informe um email válido!   |
            |                     | 1234        | Oops. Informe um email válido!   |
            | daniel@gmail.com    |             | Oops. Informe sua senha secreta! |

